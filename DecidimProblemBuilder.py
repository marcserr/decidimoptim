import os

class DecidimProblemBuilder:
    #Given a range of budgets this class will build all the LP files for the proposal selection problems with those
    #budgets

    def __init__(self, original, minb, maxb, increment, weights, buildpath):
        self.minbudget = minb #The minimum budget to start building LPs
        self.maxbudget = maxb #The maximum budget to stop building LPs
        self.increment = increment #The subsequent increment of budget between LP builds
        self.originalPath = original #Path to the original
        self.buildPath = buildpath #Directory for the generated files
        self.names = [] #List to be filled with the filenames of the generated LP files
        self.weights = weights #Problem weights to use

    def build(self):
        #This method builds each LP file, it takes an original LP file and only changes the weights and budget
        originalFile = open(self.originalPath, "r")
        fileContent = originalFile.readlines()
        originalFile.close()
        budgets = range(self.minbudget, self.maxbudget, self.increment)
        budgets.append(self.maxbudget)
        self.buildPath = self.buildPath+self.originalPath.split("/")[-1]
        for bud in budgets:
            fileContent[-1] = str(bud)
            fileContent[2] = str(self.weights[0])+"\n"
            fileContent[3] = str(self.weights[1])+"\n"
            newfilename = self.buildPath.replace(".txt", str(bud)+".txt")
            self.names.append(newfilename)
            newfile = open(newfilename, "w+")
            for line in fileContent:
                newfile.write(line)
            newfile.close()

    def getFileNames(self):
        return self.names

def main():
    datafile = raw_input("Original file:\n")
    minb = raw_input("Minimum budget to check:\n")
    maxb = raw_input("Maximum budget to check:\n")
    increment = raw_input("Increments to make:\n")
    weight1 = float(raw_input("Support weight:\n"))
    weight2 = float(raw_input("Cost weight:\n"))
    builder = DecidimProblemBuilder(datafile, minb, maxb, increment,[weight1, weight2], os.getcwd()+"Data/")
    builder.build()


if __name__ == "__main__":
        main()