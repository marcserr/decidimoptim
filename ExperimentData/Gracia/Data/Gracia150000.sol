<?xml version = "1.0" encoding="UTF-8" standalone="yes"?>
<CPLEXSolution version="1.2">
 <header
   problemName="/Users/Marc/PycharmProjects/DecidimOptim/ExperimentData/Gracia/Data/Gracia150000.lp"
   solutionName="incumbent"
   solutionIndex="-1"
   objectiveValue="6.9285714199999999"
   solutionTypeValue="3"
   solutionTypeString="primal"
   solutionStatusValue="101"
   solutionStatusString="integer optimal solution"
   solutionMethodString="mip"
   primalFeasible="1"
   dualFeasible="1"
   MIPNodes="0"
   MIPIterations="3"
   writeLevel="1"/>
 <quality
   epInt="1.0000000000000001e-05"
   epRHS="9.9999999999999995e-07"
   maxIntInfeas="0"
   maxPrimalInfeas="0"
   maxX="1"
   maxSlack="500"/>
 <linearConstraints>
  <constraint name="c1" index="0" slack="500"/>
  <constraint name="c2" index="1" slack="-7"/>
  <constraint name="c3" index="2" slack="7"/>
 </linearConstraints>
 <variables>
  <variable name="p_1" index="0" value="1"/>
  <variable name="p_2" index="1" value="1"/>
  <variable name="p_3" index="2" value="1"/>
  <variable name="p_4" index="3" value="0"/>
  <variable name="p_5" index="4" value="0"/>
  <variable name="p_6" index="5" value="1"/>
  <variable name="p_7" index="6" value="1"/>
  <variable name="p_8" index="7" value="0"/>
  <variable name="p_9" index="8" value="0"/>
  <variable name="p_10" index="9" value="1"/>
  <variable name="p_11" index="10" value="1"/>
  <variable name="p_12" index="11" value="1"/>
  <variable name="p_13" index="12" value="0"/>
  <variable name="p_14" index="13" value="0"/>
  <variable name="y" index="14" value="1"/>
 </variables>
</CPLEXSolution>
