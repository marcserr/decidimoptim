import matplotlib.pyplot as plt
import os

class ExperimentPlot:
    #Plots two lines given by two input files

    def __init__(self, input1, input2, type):
        self.input1 = open(input1, "r") #First input file
        self.input2 = open(input2, "r") #Second input file
        self.type = type #Type of plot (sup, cost or num)


    def plot(self, show = True, output = None, originalBud = None, maxdif = True):
        #Plots the lines of the input files and saves the figure in the output file (you can find the plots in the
        #"Plots" folder on each of the Eixample and Gracia directories)
        x1 = []
        y1 = []
        x2 = []
        y2 = []
        for line in self.input1:
            el = line.split(",")
            x1.append(float(el[0]))
            y1.append(float(el[1]))

        for line in self.input2:
            el = line.split(",")
            x2.append(float(el[0]))
            y2.append(float(el[1]))

        sum1 = float(sum(y1))
        sum2 = float(sum(y2))

        if self.type == "sup":
            print "Average difference in support: "+str((sum2-sum1)/len(y1))
        elif self.type == "cost":
            print "Average difference in allocated budget: " + str((sum2 - sum1) / len(y1))
        elif self.type == "num":
            print "Average difference in number of proposals selected: " + str((sum2 - sum1) / len(y1))

        if originalBud:
            y3 = range(0, int(max(y1)), 1)
            x3 = [originalBud]*len(y3)
            plt.plot(x3, y3, color="purple")

        if maxdif:
            maxdifbud = 0
            maxdif = 0
            for i in range(0,len(y1)):
                dif = y2[i]-y1[i]
                if dif >= maxdif:
                    maxdif = dif
                    maxdifbud = x1[i]
            y4 = range(0, int(max(y1)), 1)
            x4 = [maxdifbud] * len(y4)
            plt.plot(x4, y4, color="orange")

        plt.plot(x1, y1, '-o', color = "red", label = "Rank and select")
        plt.plot(x2, y2, '-o', color = "blue", label = "Optimal")
        if self.type == "sup":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Supports of the selected proposals")
        elif self.type == "cost":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Spent money")
        elif self.type == "num":
            plt.xlabel("Avaliable budget")
            plt.ylabel("Number of proposals selected")
        plt.legend(loc = 'lower right')
        if output:
            plt.savefig(output)
        if show:
            plt.show()

def main():
    i1 = "/Users/Marc/PycharmProjects/NormNetOptimisation/Decidim/ExperimentData/Gracia/Actual/DATA_SUP.txt"
    i2 = "/Users/Marc/PycharmProjects/NormNetOptimisation/Decidim/ExperimentData/Gracia/Optimal/DATA_SUP.txt"
    ep = ExperimentPlot(i1, i2, "sup")
    ep.plot()


if __name__ == "__main__":
        main()