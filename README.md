# DecidimOptim

The aim of this project is to compare proposal selection using optimisation and with the "rank and select" method. The code uses the data generated during the participatory budget process in Decidim Barcelona (namely the process in Eixample and Gracia neighbourhoods). The code solves proposal selection problems in various budget scenarios and then compares the solving methodologies based on the number of total citizen supports earned, the allocated budget and the number of selected proposal. The output is plotted to make it more understandable.

Please run DecidimPlot.py and follow these instructions:

- Select the database "Eixample" or "Gracia"

- Provide a minimum budget (for the paper we used 0)

- Provide a maximum budget (for the paper we used the minimum budget necessary to select all proposals, which is 910000 for the Eixample and 300000 for Gracia)

- Provide the original budget of the participation process (Eixample: 500000, Gracia: 150000)

- Finally, input the subsequent increments of budget between experiments, the smaller this number the larger the number of problem instances will be (for the paper we used increments of 10000) 

The program will show plots for support, allocated budget and number of proposals (please close each plot to reveal the following one) as well as providing average differences between the methods.